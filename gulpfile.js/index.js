
  'use strict';
/*!
GULP DEPENDENCIES
npm install -g gulp require-dir gulp-concat  gulp-uglify gulp-autoprefixer sass gulp-sass gulp-useref gulp-notify gulp-plumber gulp-zip gulp-bump gulp-confirm gulp-dom gulp-clean fs path gulp4-run-sequence browser-sync

sudo npm link gulp require-dir gulp-concat  gulp-uglify gulp-autoprefixer sass gulp-sass gulp-useref gulp-notify gulp-plumber gulp-zip gulp-bump gulp-confirm gulp-dom gulp-clean fs path gulp4-run-sequence browser-sync
*/

  var gulp = require('gulp'),
   requireDir = require('require-dir'),
   runSequence = require('gulp4-run-sequence'),
   tasks = requireDir('./tasks',{recurse:true});



/*--------------------------------------------------
define gulp tasks
--------------------------------------------------*/
  gulp.task('version',tasks.version);
  gulp.task('developer',tasks.developer);
  gulp.task('server',tasks.server);

  gulp.task('move:moveFiles',tasks.move.moveFiles);
  gulp.task('move:moveHTML',tasks.move.moveHTML);
  gulp.task('scripts:concat',tasks.scripts.concat);
  gulp.task('scripts:movePolite',tasks.scripts.movePolite);
  gulp.task('sass',tasks.sass);
  gulp.task('sync:applySync',tasks.sync.applySync);
  gulp.task('sync:bsReload',tasks.sync.bsReload);
  gulp.task('watchFiles',tasks.watchFiles);
  gulp.task('allTasks',allTasks);

  function allTasks (done){
    runSequence(['move:moveFiles', 'move:moveHTML', 'watchFiles'], ['scripts:concat', 'scripts:movePolite', 'sass'], ['sync:applySync'],done);
 }



 gulp.task('clean',tasks.clean);

 gulp.task('publish',gulp.series('clean',tasks.publish))

 gulp.task('publishft',gulp.series('clean',tasks.publishft));



 
/*--------------------------------------------------
define resizes tasks
--------------------------------------------------*/

gulp.task('bumpToV1',tasks.bumpToV1);
gulp.task('rmArchive',tasks.rmArchive);
gulp.task('resizeTasks:getBannerWidth',tasks.resizeTasks.getBannerWidth);
gulp.task('resizeTasks:getBannerHeight',tasks.resizeTasks.getBannerHeight);
gulp.task('resizeTasks:resizeInSass',tasks.resizeTasks.resizeInSass);
gulp.task('resizeTasks:insertDomResize',tasks.resizeTasks.insertDomResize);


gulp.task('resize',resize);

function resize(done){
  runSequence(['bumpToV1'], ['rmArchive', 'resizeTasks:getBannerWidth', 'resizeTasks:getBannerHeight', 'developer'], ['resizeTasks:resizeInSass', 'resizeTasks:insertDomResize', 'allTasks'],done);
}



/*--------------------------------------------------
DEFAULT
--------------------------------------------------*/

  gulp.task('default', gulp.series('server','version','developer', (done) => {
    allTasks();
    done();
  }));




