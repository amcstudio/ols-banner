var gulp = require('gulp'),
plumber = require('gulp-plumber'),
sass = require('gulp-sass')(require('sass')),
autoprefixer = require('gulp-autoprefixer'),
browserSync = require('browser-sync'),
fs = require('fs'),
notify = require('gulp-notify');



module.exports = function (){
    return gulp.src(['./src/scss/main.scss'])
    .pipe(plumber())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      cascade: false
    }))
    .pipe(gulp.dest('src/css'))
    .pipe(notify("Sass task complete"))
}