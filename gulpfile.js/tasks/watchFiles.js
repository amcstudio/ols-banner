  var gulp = require('gulp');


  module.exports = function (done){
  
    gulp.watch('src/scss/**/*.scss', gulp.series('sass','sync:bsReload'));
    gulp.watch('src/js/**/*.js', gulp.series('scripts:concat', 'scripts:movePolite','sync:bsReload'));
    gulp.watch(['src/**/*.*'], gulp.series('move:moveFiles'));  
    gulp.watch('src/index.html', gulp.series('move:moveHTML'));
    gulp.watch('deploy/**/**/*', gulp.series('sync:bsReload'));

    done();
  
}