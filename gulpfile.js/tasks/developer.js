var gulp = require('gulp'), 
    confirm = require('gulp-confirm'),
    fs = require('fs'),
    json,
    developerName;


  module.exports = function () {

      json = JSON.parse(fs.readFileSync('./package.json'))
      return gulp.src('src/**/*')
        .pipe(confirm({
          question: 'Developer name? (press enter if you are re-opening the same file)',
          proceed: function (answer) {
            developerName = answer;
            fs.appendFile('.contributors', '\n' + developerName + ' | v-' + json.version + ' | ' + datetime, function (err) { });
            return true;
          }
        }))
   
  };




 // /*--------------------------------------------------
  // DATE TIME FUNCTION FOR ARCHIVE NAMING
  // --------------------------------------------------*/

  var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"]
  var hrs = ["12am", "1am", "2am", "3am", "4am", "5am", "6am", "7am", "8am", "9am", "10am", "11am", "12pm", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm", "8pm", "9pm", "10pm", "11pm", "12pm"]
  var currentdate = new Date();
  var datetime = currentdate.getDate() + " " + months[currentdate.getMonth()] + " " + currentdate.getFullYear() + " at " + hrs[currentdate.getHours()];

