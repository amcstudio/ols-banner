var gulp = require('gulp');
var clean = require('gulp-clean');

module.exports = function(done){
   
    return gulp.src('./_versionArchive/', {read: false, allowEmpty:true})
    .pipe(clean());
    done();
}
